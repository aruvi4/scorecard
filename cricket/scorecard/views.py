from django.shortcuts import render, redirect
from .models import Match, Ball, Player, Over

# Create your views here.

def increment_overs(overs: str) -> tuple[str, str]:
    fullovers, balls = [int(_) for _ in overs.split(".")]
    if fullovers == 49 and balls == 5:
        return "50.0", True
    else:
        if balls == 5:
            return f"{fullovers + 1}.0", False
        else:
            return f"{fullovers}.{balls + 1}", False

def display_scorecard(request, match_id):
    match = Match.objects.get(id=match_id)
    bowling_change_required = match.current_inning.current_ball().ball_number % 6 == 0
    batting_change_required = match.current_inning.current_ball().is_wicket
    context = {'match': match,
               'bowling_change_required': bowling_change_required,
               'batting_change_required': batting_change_required}
    return render(request, 'scorecard/scorecard.html', context)

def change_team(team: str) -> str:
    return "aus" if team == "ind" else "ind"

def should_change_batsman(prev_ball:Ball) -> bool:
    print(prev_ball)
    if not prev_ball.is_legal:
        return False
    if prev_ball.ball_number % 6 == 0:
        return prev_ball.runs_scored % 2 == 0
    else:
        return prev_ball.runs_scored % 2 != 0

def update_scorecard(request, match_id):
    event = request.POST['event']
    current_match = Match.objects.get(id=match_id)
    current_inning = current_match.current_inning
    old_ball = current_inning.current_ball()
    old_over = current_inning.current_over()
    
    if request.POST.get('newbatsman'):
        batsman = Player.objects.get(first_name=request.POST.get('newbatsman'))
    else:
        batsman = old_ball.batsman

    if event == 'wd' or event == 'nb':
        new_ball_number = old_ball.ball_number
        new_runs_scored = 1
        new_is_legal = False
        new_is_wicket = False
        new_bowler = old_ball.bowler
        new_nonstriker = old_ball.nonstriker
        new_batsman = batsman
        new_comment = event
        new_bowler.runs_conceded += 1
    else:
        new_ball_number = old_ball.ball_number + 1
        new_is_legal = True
        if request.POST.get('newbowler'):
            new_bowler = Player.objects.get(first_name=request.POST.get('newbowler'))
            new_over = Over.objects.create(
                over_number=old_over.over_number + 1,
                bowler=new_bowler
            )
            current_inning.overs.add(new_over)
            current_inning.save()
        else:
            new_bowler = old_ball.bowler
        new_bowler.balls_bowled += 1
        if event == 'w':
            new_is_wicket = True
            new_runs_scored = 0
            batsman.balls_faced += 1
            batsman.wicket_comment = f"b {new_bowler}"
            new_nonstriker = old_ball.nonstriker
            new_comment = "WICKET!"
            new_bowler.wickets_taken += 1
            new_batsman = batsman
        else:
            new_is_wicket = False
            new_runs_scored = int(event)
            #batsman.runs_scored += new_runs_scored
            batsman.balls_faced += 1
            new_bowler.runs_conceded += new_runs_scored
            if should_change_batsman(current_inning.current_ball()):
                new_batsman, new_nonstriker = old_ball.nonstriker, batsman
            else:
                new_batsman, new_nonstriker = batsman, old_ball.nonstriker
            new_comment = event
            
        
    new_ball = Ball.objects.create(
        ball_number = new_ball_number,
        bowler = new_bowler,
        batsman = new_batsman,
        nonstriker = new_nonstriker,
        runs_scored = new_runs_scored,
        is_wicket = new_is_wicket,
        is_legal = new_is_legal,
        comment = new_comment
    )
    old_ball.bowler.save()
    old_ball.batsman.save()
    batsman.save()
    new_bowler.save()
    new_batsman.save()

    current_inning.current_over().balls.add(new_ball)

    return redirect('display_scorecard', match_id)

def clear_session(request):
    request.session.clear()
    return redirect("display_scorecard")