from django.db import models

# Create your models here.

class Player(models.Model):
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    #runs_scored = models.IntegerField(blank=True, null=True, default=0)
    balls_faced = models.IntegerField(blank=True, null=True, default=0)
    runs_conceded = models.IntegerField(blank=True, null=True, default=0)
    balls_bowled = models.IntegerField(blank=True, null=True, default=0)
    wickets_taken = models.IntegerField(blank=True, null=True, default=0)
    maidens = models.IntegerField(blank=True, null=True, default=0)
    fours = models.IntegerField(blank=True, null=True, default=0)
    sixes = models.IntegerField(blank=True, null=True, default=0)

    wicket_comment = models.CharField(max_length=256, default="not out")

    def overs_bowled(self) -> str:
        overs, balls = divmod(self.balls_bowled, 6)
        return f"{overs}.{balls}"
    
    def __str__(self):
        return f"{self.first_name} {self.last_name}"
    
    def batting_summary(self):
        return f"{self.first_name} {self.last_name} {self.wicket_comment} {self.runs_scored()} ({self.balls_faced})"
    
    def bowling_summary(self):
        return f"{self.first_name} {self.last_name} {self.overs_bowled()} - {self.maidens} - {self.runs_conceded} - {self.wickets_taken}"

    def runs_scored(self):
        balls_faced_by_self = Ball.objects.filter(batsman=self)
        return sum(ball.runs_scored for ball in balls_faced_by_self)

class Team(models.Model):

    BATTING_ORDER_CHOICES = [("first", "first"), ("second", "second")]

    name = models.CharField(max_length=256)
    players = models.ManyToManyField(Player)
    batting_order = models.CharField(choices=BATTING_ORDER_CHOICES, max_length=10)

    def __str__(self):
        return self.name

class Ball(models.Model):
    ball_number = models.IntegerField()
    bowler = models.ForeignKey(Player, on_delete=models.CASCADE, related_name="bowler")
    batsman = models.ForeignKey(Player, on_delete=models.CASCADE, related_name="batsman")
    nonstriker = models.ForeignKey(Player, on_delete=models.CASCADE, related_name="nonstriker", null=True)
    runs_scored = models.IntegerField()
    is_wicket = models.BooleanField()
    is_legal = models.BooleanField()
    comment = models.CharField(max_length=256)

    def as_overs(self):
        overs, balls = divmod(self.ball_number, 6)
        return f"{overs}.{balls}"

    def __str__(self):
        if not self.is_wicket:
            message = f"{self.runs_scored} runs scored"
        else:
            message = "wicket!"
        return f"{self.as_overs()} {self.bowler} to {self.batsman} - {message}"

class Over(models.Model):
    over_number = models.IntegerField()
    bowler = models.ForeignKey(Player, on_delete=models.CASCADE)
    balls = models.ManyToManyField(Ball)
    
    def overs_bowled(self) -> str:
        return f"{self.over_number - 1}.{len([b for b in self.balls.all() if b.is_legal])}"

    def total_runs(self) -> int:
        return sum(b.runs_scored for b in self.balls.all())
    
    def total_wickets(self) -> int:
        return len([b for b in self.balls.all() if b.is_wicket])
    
    def __str__(self):
        return f"{self.overs_bowled()}"

class Inning(models.Model):
    #total_runs = models.IntegerField()
    overs = models.ManyToManyField(Over)

    batting_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="batting_team")
    bowling_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="bowling_team")

    #current_over = models.ForeignKey(Over, on_delete=models.CASCADE)
    #current_ball = models.ForeignKey(Ball, on_delete=models.CASCADE)
    def total_wickets(self):
        return sum(over.total_wickets() for over in self.overs.all())
    
    def total_runs(self):
        return sum(over.total_runs() for over in self.overs.all())
    
    def current_over(self):
        return max(self.overs.all(), key=lambda o: o.over_number)
    
    def current_ball(self):
        return max(self.current_over().balls.all(), key=lambda b: b.ball_number)
    
    def __str__(self):
        return f"{self.batting_team} {self.total_runs()} - {self.total_wickets()} {self.current_over()}"

    def prev_ball(self):
        current_ball_number = self.current_ball().ball_number
        prev_ball_number = current_ball_number - 1
        return Ball.objects.get(ball_number = prev_ball_number)



class Match(models.Model):
    team1 = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="home_team")
    team2 = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="away_team")
    all_innings = models.ManyToManyField(Inning)
    current_inning = models.ForeignKey(Inning, on_delete=models.CASCADE, related_name="current_inning")

    def other_innings(self):
        return [inning for inning in self.all_innings.all() if inning != self.current_inning]
    
    def other_team(self):
        if self.current_inning.batting_team == self.team1:
            return self.team2
        else:
            return self.team1
    
    def is_finished(self):
        return False
