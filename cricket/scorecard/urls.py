from django.urls import path
from . import views

urlpatterns = [
    path('display/<int:match_id>', views.display_scorecard, name='display_scorecard'),
    path('update/<int:match_id>', views.update_scorecard, name='update_scorecard'),
    path('clear', views.clear_session, name='clear_session')
]