from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register([
    Player, Team, Ball, Over, Inning, Match
])