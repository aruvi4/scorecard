# Generated by Django 4.2.2 on 2023-06-22 08:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('scorecard', '0001_squashed_0005_remove_inning_balls_remove_inning_current_batsman_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='ball',
            name='nonstriker',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='nonstriker', to='scorecard.player'),
        ),
    ]
