# Generated by Django 4.2.2 on 2023-06-20 07:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scorecard', '0004_alter_player_balls_bowled_alter_player_balls_faced_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='inning',
            name='balls',
        ),
        migrations.RemoveField(
            model_name='inning',
            name='current_batsman',
        ),
        migrations.RemoveField(
            model_name='inning',
            name='current_over',
        ),
        migrations.RemoveField(
            model_name='inning',
            name='total_runs',
        ),
        migrations.AddField(
            model_name='inning',
            name='overs',
            field=models.ManyToManyField(to='scorecard.over'),
        ),
    ]
