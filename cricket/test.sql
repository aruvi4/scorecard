BEGIN;
--
-- Create model Ball
--
CREATE TABLE "scorecard_ball" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "ball_number" integer NOT NULL, "runs_scored" integer NOT NULL, "is_wicket" bool NOT NULL, "is_legal" bool NOT NULL, "comment" varchar(256) NOT NULL);
--
-- Create model Inning
--
CREATE TABLE "scorecard_inning" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "total_runs" integer NOT NULL);
CREATE TABLE "scorecard_inning_balls" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "inning_id" bigint NOT NULL REFERENCES "scorecard_inning" ("id") DEFERRABLE INITIALLY DEFERRED, "ball_id" bigint NOT NULL REFERENCES "scorecard_ball" ("id") DEFERRABLE INITIALLY DEFERRED);
--
-- Create model Player
--
CREATE TABLE "scorecard_player" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "first_name" varchar(256) NOT NULL, "last_name" varchar(256) NOT NULL, "runs_scored" integer NULL, "balls_faced" integer NULL, "runs_conceded" integer NULL, "balls_bowled" integer NULL, "wickets_taken" integer NULL, "maidens" integer NULL, "fours" integer NULL, "sixes" integer NULL);
--
-- Create model Team
--
CREATE TABLE "scorecard_team" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(256) NOT NULL, "batting_order" varchar(10) NOT NULL);
CREATE TABLE "scorecard_team_players" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "team_id" bigint NOT NULL REFERENCES "scorecard_team" ("id") DEFERRABLE INITIALLY DEFERRED, "player_id" bigint NOT NULL REFERENCES "scorecard_player" ("id") DEFERRABLE INITIALLY DEFERRED);
--
-- Create model Over
--
CREATE TABLE "scorecard_over" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "over_number" integer NOT NULL, "bowler_id" bigint NOT NULL UNIQUE REFERENCES "scorecard_player" ("id") DEFERRABLE INITIALLY DEFERRED);
CREATE TABLE "scorecard_over_balls" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "over_id" bigint NOT NULL REFERENCES "scorecard_over" ("id") DEFERRABLE INITIALLY DEFERRED, "ball_id" bigint NOT NULL REFERENCES "scorecard_ball" ("id") DEFERRABLE INITIALLY DEFERRED);
--
-- Create model Match
--
CREATE TABLE "scorecard_match" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "current_inning_id" bigint NOT NULL REFERENCES "scorecard_inning" ("id") DEFERRABLE INITIALLY DEFERRED, "team1_id" bigint NOT NULL REFERENCES "scorecard_team" ("id") DEFERRABLE INITIALLY DEFERRED, "team2_id" bigint NOT NULL REFERENCES "scorecard_team" ("id") DEFERRABLE INITIALLY DEFERRED);
CREATE TABLE "scorecard_match_all_innings" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "match_id" bigint NOT NULL REFERENCES "scorecard_match" ("id") DEFERRABLE INITIALLY DEFERRED, "inning_id" bigint NOT NULL REFERENCES "scorecard_inning" ("id") DEFERRABLE INITIALLY DEFERRED);
--
-- Add field batting_team to inning
--
CREATE TABLE "new__scorecard_inning" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "total_runs" integer NOT NULL, "batting_team_id" bigint NOT NULL REFERENCES "scorecard_team" ("id") DEFERRABLE INITIALLY DEFERRED);
INSERT INTO "new__scorecard_inning" ("id", "total_runs", "batting_team_id") SELECT "id", "total_runs", NULL FROM "scorecard_inning";
DROP TABLE "scorecard_inning";
ALTER TABLE "new__scorecard_inning" RENAME TO "scorecard_inning";
CREATE UNIQUE INDEX "scorecard_inning_balls_inning_id_ball_id_2b71d6aa_uniq" ON "scorecard_inning_balls" ("inning_id", "ball_id");
CREATE INDEX "scorecard_inning_balls_inning_id_e063bee4" ON "scorecard_inning_balls" ("inning_id");
CREATE INDEX "scorecard_inning_balls_ball_id_634839ee" ON "scorecard_inning_balls" ("ball_id");
CREATE UNIQUE INDEX "scorecard_team_players_team_id_player_id_38074f88_uniq" ON "scorecard_team_players" ("team_id", "player_id");
CREATE INDEX "scorecard_team_players_team_id_05e8c92d" ON "scorecard_team_players" ("team_id");
CREATE INDEX "scorecard_team_players_player_id_7302e025" ON "scorecard_team_players" ("player_id");
CREATE UNIQUE INDEX "scorecard_over_balls_over_id_ball_id_f2d2cfbe_uniq" ON "scorecard_over_balls" ("over_id", "ball_id");
CREATE INDEX "scorecard_over_balls_over_id_3ff5b766" ON "scorecard_over_balls" ("over_id");
CREATE INDEX "scorecard_over_balls_ball_id_86a56d6a" ON "scorecard_over_balls" ("ball_id");
CREATE INDEX "scorecard_match_current_inning_id_f0833822" ON "scorecard_match" ("current_inning_id");
CREATE INDEX "scorecard_match_team1_id_f7a88323" ON "scorecard_match" ("team1_id");
CREATE INDEX "scorecard_match_team2_id_e699a0cf" ON "scorecard_match" ("team2_id");
CREATE UNIQUE INDEX "scorecard_match_all_innings_match_id_inning_id_ad4cb316_uniq" ON "scorecard_match_all_innings" ("match_id", "inning_id");
CREATE INDEX "scorecard_match_all_innings_match_id_03d8c944" ON "scorecard_match_all_innings" ("match_id");
CREATE INDEX "scorecard_match_all_innings_inning_id_b335eb17" ON "scorecard_match_all_innings" ("inning_id");
CREATE INDEX "scorecard_inning_batting_team_id_a91bba3e" ON "scorecard_inning" ("batting_team_id");
--
-- Add field bowling_team to inning
--
CREATE TABLE "new__scorecard_inning" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "total_runs" integer NOT NULL, "batting_team_id" bigint NOT NULL REFERENCES "scorecard_team" ("id") DEFERRABLE INITIALLY DEFERRED, "bowling_team_id" bigint NOT NULL REFERENCES "scorecard_team" ("id") DEFERRABLE INITIALLY DEFERRED);
INSERT INTO "new__scorecard_inning" ("id", "total_runs", "batting_team_id", "bowling_team_id") SELECT "id", "total_runs", "batting_team_id", NULL FROM "scorecard_inning";
DROP TABLE "scorecard_inning";
ALTER TABLE "new__scorecard_inning" RENAME TO "scorecard_inning";
CREATE INDEX "scorecard_inning_batting_team_id_a91bba3e" ON "scorecard_inning" ("batting_team_id");
CREATE INDEX "scorecard_inning_bowling_team_id_16322042" ON "scorecard_inning" ("bowling_team_id");
--
-- Add field batsman to ball
--
CREATE TABLE "new__scorecard_ball" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "ball_number" integer NOT NULL, "runs_scored" integer NOT NULL, "is_wicket" bool NOT NULL, "is_legal" bool NOT NULL, "comment" varchar(256) NOT NULL, "batsman_id" bigint NOT NULL UNIQUE REFERENCES "scorecard_player" ("id") DEFERRABLE INITIALLY DEFERRED);
INSERT INTO "new__scorecard_ball" ("id", "ball_number", "runs_scored", "is_wicket", "is_legal", "comment", "batsman_id") SELECT "id", "ball_number", "runs_scored", "is_wicket", "is_legal", "comment", NULL FROM "scorecard_ball";
DROP TABLE "scorecard_ball";
ALTER TABLE "new__scorecard_ball" RENAME TO "scorecard_ball";
--
-- Add field bowler to ball
--
CREATE TABLE "new__scorecard_ball" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "ball_number" integer NOT NULL, "runs_scored" integer NOT NULL, "is_wicket" bool NOT NULL, "is_legal" bool NOT NULL, "comment" varchar(256) NOT NULL, "batsman_id" bigint NOT NULL UNIQUE REFERENCES "scorecard_player" ("id") DEFERRABLE INITIALLY DEFERRED, "bowler_id" bigint NOT NULL UNIQUE REFERENCES "scorecard_player" ("id") DEFERRABLE INITIALLY DEFERRED);
INSERT INTO "new__scorecard_ball" ("id", "ball_number", "runs_scored", "is_wicket", "is_legal", "comment", "batsman_id", "bowler_id") SELECT "id", "ball_number", "runs_scored", "is_wicket", "is_legal", "comment", "batsman_id", NULL FROM "scorecard_ball";
DROP TABLE "scorecard_ball";
ALTER TABLE "new__scorecard_ball" RENAME TO "scorecard_ball";
--
-- Remove field balls from inning
--
DROP TABLE "scorecard_inning_balls";
--
-- Remove field total_runs from inning
--
ALTER TABLE "scorecard_inning" DROP COLUMN "total_runs";
--
-- Add field overs to inning
--
CREATE TABLE "scorecard_inning_overs" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "inning_id" bigint NOT NULL REFERENCES "scorecard_inning" ("id") DEFERRABLE INITIALLY DEFERRED, "over_id" bigint NOT NULL REFERENCES "scorecard_over" ("id") DEFERRABLE INITIALLY DEFERRED);
CREATE UNIQUE INDEX "scorecard_inning_overs_inning_id_over_id_232a3bf1_uniq" ON "scorecard_inning_overs" ("inning_id", "over_id");
CREATE INDEX "scorecard_inning_overs_inning_id_285db18b" ON "scorecard_inning_overs" ("inning_id");
CREATE INDEX "scorecard_inning_overs_over_id_27889703" ON "scorecard_inning_overs" ("over_id");
COMMIT;
